Publications DiCosmo over the last five years
---------------------------------------------

Cyrille Artho, Roberto Di Cosmo, Kuniyasu Suzaki, Ralf Treinen,
Stefano Zacchiroli. Why Do Software Packages Conflict? 9th Working
Conference on Mining Software Repositories, June 2-3. Zurich,
Switzerland.

Marco Danelutto, Roberto Di Cosmo: A "Minimal Disruption" Skeleton
Experiment: Seamless Map & Reduce Embedding in OCaml. Procedia CS 9:
1837-1846 (2012)

Pietro Abate, Roberto Di Cosmo, Ralf Treinen, Stefano
Zacchiroli. Learning from the Future of Component Repositories. 15th
International ACM SIGSOFT Symposium on Component Based Software
Engineering (CBSE-2012), Bertinoro, Italy, June 26-28, 2012.

Pietro Abate, Roberto Di Cosmo: Predicting upgrade failures using
dependency analysis. Workshops Proceedings of the 27th International
Conference on Data Engineering, ICDE 2011, Hannover, Germany 2011:
pages 145-150

Roberto Di Cosmo, Olivier Lhomme, Claude Michel: Aligning component
upgrades. Proceedings of the second International Workshop on Logics
for Component Configuration 2011, Electronic Proceedings in Computer
Science (EPTCS) vol 65, pages 1-11

Pietro Abate, Roberto Di Cosmo, Ralf Treinen, Stefano Zacchiroli. MPM:
a modular package manager. 14th International ACM SIGSOFT Symposium
on Component Based Software Engineering (CBSE-2011), June 21th - 23th,
2011 -- Boulder, Colorado, USA.

Roberto Di Cosmo, Jérôme Vouillon: On software component co-installability.
SIGSOFT FSE 2011: 19th ACM SIGSOFT Symposium on the Foundations of Software
Engineering (FSE-19) and ESEC'11: 13rd European Software Engineering Conference
(ESEC-13), Szeged, Hungary, September 5-9, 2011

Roberto Di Cosmo, Davide Di Ruscio, Patrizio Pelliccione, Alfonso
Pierantonio, Stefano Zacchiroli: Supporting software evolution in
component-based FOSS systems. Sci. Comput. Program. 76(12): 1144-1160
(2011)

Roberto Di Cosmo, Jaap Boender: Using strong conflicts to detect
quality issues in component-based complex systems. Proceeding of the
3rd Annual India Software Engineering Conference (ICSE 2010): 163-172

Mariangiola Dezani-Ciancaglini, Roberto Di Cosmo, Elio Giovannetti,
Makoto Tatsuta: On isomorphisms of intersection types. ACM
Trans. Comput. Log. 11(4): (2010)

Pietro Abate, Roberto Di Cosmo, Jaap Boender, Stefano Zacchiroli: Strong
dependencies between software components.  Proceedings of the Third
International Symposium on Empirical Software Engineering and Measurement, ESEM
2009, October 15-16, 2009, Lake Buena Vista, Florida, USA

Mariangiola Dezani-Ciancaglini, Roberto Di Cosmo, Elio Giovannetti,
Makoto Tatsuta: On Isomorphisms of Intersection Types. CSL 2008:
461-477

Roberto Di Cosmo, Stefano Zacchiroli, Paulo Trezentos: Package
Upgrades In FOSS Distributions: Details And Challenges. Proceedings of
the HotSWUp Workshop 2008

Roberto Di Cosmo, Zheng Li, Susanna Pelagatti, Pierre Weis: Skeletal
Parallel Programming with OCamlP3l 2.0. Parallel Processing Letters
18(1): 149-164 (2008)

Roberto Di Cosmo, Zheng Li, Susanna Pelagatti: A calculus for parallel
computations over multidimensional dense arrays. Computer Languages,
Systems & Structures 33(3-4): 82-110 (2007)


